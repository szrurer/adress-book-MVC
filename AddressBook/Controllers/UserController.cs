﻿using AddressBook.Models;
using AddressBook.Utils;
using AdresKayitMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AddressBook.Controllers
{
    public class UserController : Controller
    {
        AddressBookEntities db = new AddressBookEntities();
        AES256CBC aesClass = new AES256CBC();
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoginCancel()
        {
            Session.Abandon();
            return View("Login");
        }
        [HttpPost]
        public ActionResult Login(Users users)
        {

            string userName = aesClass.EncryptStringToBytes_Aes(users.UserName);
            string password = aesClass.EncryptStringToBytes_Aes(users.Password);
            Users u = db.Users.FirstOrDefault(x => x.Password == password && x.UserName == userName);
            if (u != null)
            {
                //byte[] bArray = Encoding.ASCII.GetBytes(u.UserName);
                //string UserName = aesClass.DecryptStringFromBytes_Aes(bArray);
                Session["Email"] = u.Email;
                Session["Id"] = u.Id;
                Session.Timeout = 10;


                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Error = "Kullanıcı Adı yada Şifre Yanlış";
                return View();
            }

        }
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(Users users)
        {
            Users u = db.Users.FirstOrDefault(x => x.Email == users.Email);
            if (u == null)
            {
                users.Name = aesClass.EncryptStringToBytes_Aes(users.Name);
                users.SurName = aesClass.EncryptStringToBytes_Aes(users.SurName);
                users.UserName = aesClass.EncryptStringToBytes_Aes(users.UserName);
                users.Password = aesClass.EncryptStringToBytes_Aes(users.Password);

                db.Users.Add(users);
                db.SaveChanges();
                //byte[] bArray = Encoding.ASCII.GetBytes(users.UserName);
                //string UserName = aesClass.DecryptStringFromBytes_Aes(bArray);
                Session["Email"] = users.Email;
                Session["Id"] = users.Id;
                Session.Timeout = 10;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.ErrorMail = "Bu mail adresi daha önce kayıt edilmiştir. Lütfen farklı bir tane deneyiniz.";
                return View("Register");
            }
        }
        public ActionResult UsersList()
        {
            List<Users> users = db.Users.ToList();

            foreach (Users item in users)
            {
                item.Name = aesClass.DecryptStringFromBytes_Aes(item.Name);
                item.SurName = aesClass.DecryptStringFromBytes_Aes(item.SurName);
                item.Password = aesClass.DecryptStringFromBytes_Aes(item.Password);
                item.UserName = aesClass.DecryptStringFromBytes_Aes(item.UserName);
            }
            return View(users);
        }
        [HttpPost]
        public ActionResult UsersList(List<Users> users)
        {

            return View(users);
        }
        [HttpPost]
        public ActionResult UsersListName()
        {
            List<Users> users = db.Users.ToList();
            foreach (Users item in users)
            {
                item.Name = aesClass.DecryptStringFromBytes_Aes(item.Name);
                item.SurName = aesClass.DecryptStringFromBytes_Aes(item.SurName);
                item.Password = aesClass.DecryptStringFromBytes_Aes(item.Password);
                item.UserName = aesClass.DecryptStringFromBytes_Aes(item.UserName);
            }

            return View("UsersList", users.OrderBy(x => x.Name).ToList());
        }
        [HttpPost]
        public ActionResult UsersListSurName()
        {
            List<Users> users = db.Users.ToList();

            foreach (Users item in users)
            {
                item.Name = aesClass.DecryptStringFromBytes_Aes(item.Name);
                item.SurName = aesClass.DecryptStringFromBytes_Aes(item.SurName);
                item.Password = aesClass.DecryptStringFromBytes_Aes(item.Password);
                item.UserName = aesClass.DecryptStringFromBytes_Aes(item.UserName);
            }

            return View("UsersList", users.OrderBy(x => x.SurName).ToList());

        }
    }
}